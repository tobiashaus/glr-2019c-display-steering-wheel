
#include <touchgfx/Font.hpp>

#ifndef NO_USING_NAMESPACE_TOUCHGFX
using namespace touchgfx;
#endif

FONT_LOCATION_FLASH_PRAGMA
KEEP extern const touchgfx::GlyphNode glyphs_verdana_100_4bpp[] FONT_LOCATION_FLASH_ATTRIBUTE =
{
    {     0,  45,  31,   9,  36,   7,  45, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   144,  48,  51,  76,  74,   6,  64, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  2120,  49,  40,  73,  73,  13,  64, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  3580,  50,  51,  74,  74,   7,  64, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  5504,  51,  49,  76,  74,   8,  64, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  7404,  52,  56,  73,  73,   3,  64, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  9448,  53,  48,  74,  72,   9,  64, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    { 11224,  54,  53,  76,  74,   6,  64, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    { 13276,  55,  51,  73,  73,   7,  64, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    { 15174,  56,  53,  76,  74,   5,  64, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    { 17226,  57,  52,  76,  75,   5,  64, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    { 19202,  59,  22,  73,  55,  11,  45, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    { 20005,  63,  43,  74,  74,   7,  55, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0}
};

