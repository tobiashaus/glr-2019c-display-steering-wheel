#include <stdint.h>
#include <touchgfx/Unicode.hpp>

#ifndef NO_USING_NAMESPACE_TOUCHGFX
using namespace touchgfx;
#endif



// Language Gb: No substitution
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId2_Gb[13] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x75, 0x6e, 0x74, 0x65, 0x6e, 0x20, 0x72, 0x65, 0x63, 0x68, 0x74, 0x73, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId4_Gb[12] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x75, 0x6e, 0x74, 0x65, 0x6e, 0x20, 0x6c, 0x69, 0x6e, 0x6b, 0x73, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId5_Gb[6] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x6d, 0x69, 0x74, 0x74, 0x65, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId6_Gb[3] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x31, 0x30, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId3_Gb[11] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x6f, 0x62, 0x65, 0x6e, 0x20, 0x6c, 0x69, 0x6e, 0x6b, 0x73, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId7_Gb[2] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x2, 0x0 };
TEXT_LOCATION_FLASH_PRAGMA
KEEP const touchgfx::Unicode::UnicodeChar T_SingleUseId8_Gb[2] TEXT_LOCATION_FLASH_ATTRIBUTE = { 0x31, 0x0 };

TEXT_LOCATION_FLASH_PRAGMA
KEEP extern const touchgfx::Unicode::UnicodeChar* const textsGb[8] TEXT_LOCATION_FLASH_ATTRIBUTE =
{
    T_SingleUseId7_Gb,
    T_SingleUseId2_Gb,
    T_SingleUseId4_Gb,
    T_SingleUseId5_Gb,
    T_SingleUseId6_Gb,
    T_SingleUseId3_Gb,
    T_SingleUseId7_Gb,
    T_SingleUseId8_Gb
};

