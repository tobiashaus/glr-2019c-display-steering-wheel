
#include <touchgfx/Font.hpp>

#ifndef NO_USING_NAMESPACE_TOUCHGFX
using namespace touchgfx;
#endif

FONT_LOCATION_FLASH_PRAGMA
KEEP extern const touchgfx::GlyphNode glyphs_verdana_20_4bpp[] FONT_LOCATION_FLASH_ATTRIBUTE =
{
    {     0,  32,   0,   0,   0,   0,   7, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {     0,  45,   7,   2,   7,   1,   9,   0,   1, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {     8,  48,  11,  14,  14,   1,  13, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {    92,  49,   9,  14,  14,   2,  13, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   162,  50,  11,  14,  14,   1,  13, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   246,  51,  11,  14,  14,   1,  13, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   330,  52,  12,  14,  14,   0,  13, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   414,  53,  11,  14,  14,   1,  13, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   498,  54,  11,  14,  14,   1,  13, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   582,  55,  11,  14,  14,   1,  13, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   666,  56,  11,  14,  14,   1,  13, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   750,  57,  11,  14,  14,   1,  13, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   834,  98,  11,  15,  15,   1,  12, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   924,  99,   9,  11,  11,   1,  10, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {   979, 101,  10,  11,  11,   1,  12, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1034, 104,  10,  15,  15,   1,  13, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1109, 105,   3,  14,  14,   1,   5, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1137, 107,  11,  15,  15,   1,  12, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1227, 108,   3,  15,  15,   1,   5, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1257, 109,  17,  11,  11,   1,  19, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1356, 110,  10,  11,  11,   1,  13, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1411, 111,  11,  11,  11,   1,  12, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1477, 114,   8,  11,  11,   1,   9, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1521, 115,   9,  11,  11,   1,  10, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1576, 116,   8,  14,  14,   0,   8, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0},
    {  1632, 117,  10,  11,  11,   1,  13, 255,   0, touchgfx::GLYPH_DATA_FORMAT_A4 | 0}
};

